'use strict';

const gulp = require('gulp'),
    rimraf = require('rimraf'),
    scss = require('gulp-sass'),
    rigger = require('gulp-rigger'),
    uglify = require('gulp-uglify'),
    prefixer = require('gulp-autoprefixer'),
    htmlmin = require('gulp-htmlmin'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload;

var path = {
    build:{
        all:'build/',
        scss:'build/css/',
        js:'build/js/',
        html:'build/',
        //img:'build/img/',
        //favicons:'build/favicon'
    },
    src:{
        scss:'src/scss/app.scss',
        js:'src/js/app.js',
        html:'src/*.{html,htm}',
        //img:'src/img/**/*.{jpg,gif,png,svg,jpeg}'
    },
    watch:{
        scss:'src/scss/.scss',
        js:'src/js/**/*.js',
        html:'src/*.{html,htm}',
        //img:'src/img/**/*.{jpg,gif,png,svg,jpeg}'
    },
    clean:'build/'
},
    config={
    server:{
        baseDir:"./build",
        index:"bar.html"
    },
        host:"localhost",
        port:7787,
        tunnel:true,
        logPrefix:"WebDev"

};

// эту часть по необходимости можно использовать
// gulp.task('test',function (done) {
//     console.log('test task');
//     done();
// });
//
// gulp.task('test:promise', function(){
//     return new Promise((resolve, reject) => {
//         console.log('test promise');
//         resolve('result');
//     });
// });
//
// gulp.task('test:stream', function(){
//     //creates nodejs stream and done(and throws data away)
//     console.log('test stream');
//     return require('fs').createReadStream(__filename);
// });
//
// gulp.task('test:process', function(){
//     //return child process
//     return require('child_process').spawn('ls', ['.'], {stdio: 'inherit'});
// });

// gulp.task('work',function(done) {
//    // gulp.src(['src/**/*.{js,scss}'])
//    // gulp.src(['src/**/*.js','src/**/*.scss'])
//     gulp.src(['src/**/{app,colors}.scss'])
//         .on('data',function (file) {
//             console.log({
//                 contents:file.contents,
//                 cwd:file.cwd,
//                 base:file.base,
//                 path:file.path,
//
//                 relative: file.relative,
//                 dirname:  file.dirname,
//                 basename: file.basename,
//                 stem:     file.stem,
//                 extname:  file.extname
//             });
//         })
//         .pipe(gulp.dest(function (file) {
//                 if(file.extname == '.scss'){
//                     file.relative = file.basename;
//                     return 'build/sass';
//                 }
//                 else if(file.extname == '.js'){
//                     return 'build/js';
//                 }
//                 else{
//                     return 'build'
//                 }
//         }));
//     done();
// });

// чистим авгиевы конюшни
gulp.task('clean',function (done) {
    rimraf(path.clean,done);
});

// запуск webserver
gulp.task('webserver',function (done) {
    browserSync(config);
    done();
});

// всё что относится к ветке dev
gulp.task('dev:html',function (done) {
    gulp.src(path.src.html)
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream:true}));
    done();
});

gulp.task('dev:scss', function (done) {
    gulp.src(path.src.scss,{sourcemaps: true})
        .pipe(scss({
            outputStyle:"compressed",
            sourcemaps:false
        }))
        .pipe(prefixer({
            cascade:false,
            remove:true
        }))
        .pipe(gulp.dest(path.build.scss,{sourcemaps:'.'}))
        .pipe(reload({stream:true}));
    done();
});

gulp.task('dev:js', function (done) {
    gulp.src(path.src.js,{sourcemaps: true})
        .pipe(rigger())
        .pipe(gulp.dest(path.build.js,{sourcemaps:'.'}))
        .pipe(reload({stream:true}));
    done();
});

// все задачи для prod
gulp.task('prod:html',function (done) {
    gulp.src(path.src.html)
        .pipe(htmlmin({
            collapseWhitespace:true
        }))
        .pipe(gulp.dest(path.build.html));
    done();
});

gulp.task('prod:scss', function (done) {
    gulp.src(path.src.scss)
        .pipe(scss({
            outputStyle:"compressed",
            sourcemaps:false
        }))
        .pipe(prefixer({
            cascade:false,
            remove:true
        }))
        .pipe(gulp.dest(path.build.scss));
    done();
});

gulp.task('prod:js', function (done) {
    gulp.src(path.src.js)
        .pipe(rigger())
        .pipe(uglify())
        .pipe(gulp.dest(path.build.js));
    done();
});

// следим за изменениями в директориях
gulp.task('watch', function (done) {
    gulp.watch(path.watch.html,gulp.series('dev:html'),reload({stream:true}));
    gulp.watch(path.watch.js,gulp.series('dev:js'),reload({stream:true}));
    gulp.watch(path.watch.scss,gulp.series('dev:scss'),reload({stream:true}));
    done();
});

// добавил код для копирования js плагинов в build
gulp.task('jq:addons', function (done){
    gulp.src('node_modules/jquery/dist/jquery.min.js')
        .pipe(gulp.dest('build/js/'));
    gulp.src('node_modules/clipboard/dist/clipboard.min.js')
        .pipe(gulp.dest('build/js/'));
    done();
});

gulp.task('prod',gulp.series('clean',gulp.parallel('prod:html','prod:scss','prod:js','jq:addons'),'webserver'));
gulp.task('dev',gulp.series('clean',gulp.parallel('dev:html','dev:scss','dev:js','jq:addons'),'webserver','watch'));

gulp.task('default',gulp.series('dev'));