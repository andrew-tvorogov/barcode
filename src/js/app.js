$(document).ready(function(){
    function cSum(){
        var str = $('#code-str').val();
        var start = String.fromCharCode(204);//start symbol
        var end = String.fromCharCode(206);//end symbol
        var sum = 104;
        for (var i=0; i<str.length; i++){
            sum = sum + (str.charCodeAt(i)-32)*(i+1);
        }
        sum %=103;
        if (sum<95){
            sum +=32;
        }
        else{
            sum+=100;
        }
        $("#code-result").text(start+str+String.fromCharCode(sum)+end);
        $('.btn-clip').attr("data-clipboard-text",$("#code-result").text());
        $('.results').removeClass("hidden").addClass('animated')
        new ClipboardJS('.btn-clip');
    }
    $(document).keypress(function(e) {
        if(e.which == 13) {
            cSum();
        }
    });
    $("#code-gen").on( "click", function() {
        cSum();
    });
});